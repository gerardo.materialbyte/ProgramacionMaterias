﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SkyNet
{
    public partial class Default : System.Web.UI.Page
    {
        SkyNetEntity cont = new SkyNetEntity();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            var busqueda = from student s in cont.students
                           where s.register_number.Contains(txtBuscar.Text)
                           select s;
            GridView1.DataSource = busqueda.ToList();
            GridView1.DataBind();
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int fila = e.NewSelectedIndex;
            Response.Redirect("Programacion.aspx?register="+GridView1.Rows[fila].Cells[0].Text);
        }
    }
}