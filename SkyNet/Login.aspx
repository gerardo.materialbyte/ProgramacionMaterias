﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SkyNet.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SkyNet.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/md5.js"></script>
    <script>
        function encriptarPassword() {
            var txtP = document.getElementById("ContentPlaceHolder1_txtPassword");
            var pe = calcMD5(txtP.value);
            txtP.value = pe;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col s12 m4 offset-m4">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
                        <span>Nombre de Usuario</span>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <span>Contraseña</span>
                    </div>
                    <div class="row">
                        <asp:Button ID="btnLogin" runat="server" OnClientClick="encriptarPassword()" Text="Entrar" CssClass="waves-effect btn-large" OnClick="btnLogin_Click" style="left: 0px; top: 0px" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
