﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SkyNet.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SkyNet.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 345px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="row">
        <div class="col s12 m5">
            <asp:TextBox ID="txtBuscar" runat="server"></asp:TextBox>
        </div>
        <div class="col s12 m4">
            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" CssClass="btn waves-effect waves-teal #00897b teal darken-1" />
        </div>
    </div>
    <div class="row">
        <div class="col s12 m8">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="responsive-table"
                OnSelectedIndexChanging="GridView1_SelectedIndexChanging" >
                    <Columns>
                        <asp:BoundField DataField="register_number" HeaderText="Numero de Registro" />
                        <asp:BoundField DataField="name" HeaderText="Nombre" />
                        <asp:BoundField DataField="father_lastname" HeaderText="Apellido Paterno" />
                        <asp:BoundField DataField="mother_lastname" HeaderText="Apellido Materno" />
                        <asp:CommandField SelectText="Ver" ShowSelectButton="True" />
                    </Columns>
                </asp:GridView>
        </div>
    </div>
</asp:Content>
