﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SkyNet
{
    public partial class Reporte : System.Web.UI.Page
    {
        SkyNetEntity cont = new SkyNetEntity();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                cargarPromedio();
                cargarCantidadEstudiantes();
                cargarMateriasOfertadas();
                cargarCantidadCarreras();
            }
        }

        public void cargarPromedio() {
            var datos = Math.Round((from historyNotes in cont.historyNotes
                                    select historyNotes.note).Average(), 2);

            lblPromedio.Text = datos.ToString();
        }
        public void cargarCantidadEstudiantes() {
            var cantidad = (from student st in cont.students
                            select st).Count();

            lblCantidadEst.Text = cantidad.ToString();
        }
        public void cargarMateriasOfertadas() {
            var cantidad = (from groupsOffered gp in cont.groupsOffereds
                            select gp).Count();

            lblCantidadMateriasOfertadas.Text = cantidad.ToString();
        }
        public void cargarCantidadCarreras() {
            var cantidad = (from career cr in cont.careers
                            select cr).Count();

            lblCantidadCarreras.Text = cantidad.ToString();
        }
    }
}