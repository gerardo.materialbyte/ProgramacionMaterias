﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SkyNet
{
    public partial class RegisterCredential : System.Web.UI.Page
    {
        SkyNetEntity con = new SkyNetEntity();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click1(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                credentialAdministrator credential = new credentialAdministrator();
                credential.name = txtNombre.Text;
                credential.lastname = txtApellido.Text;
                credential.username = txtUsuario.Text;
                credential.passwordUser = txtPassword.Text;

                con.credentialAdministrators.Add(credential);
                if (con.SaveChanges() > 0)
                {
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    Response.Redirect("RegisterCredential.aspx");
                }
            }
        }
    }
}