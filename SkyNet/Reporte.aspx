﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SkyNet.Master" AutoEventWireup="true" CodeBehind="Reporte.aspx.cs" Inherits="SkyNet.Reporte" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <h1 align="center">Reporte General De Entidad</h1>
    </div>
    <div class="row">
        <div class="col s12 m3">
            <div class="card-panel #00897b teal darken-1">
                 <div class="card-content white-text">
                     <asp:Label runat="server" Text="Materias Ofertadas"></asp:Label><i class="material-icons">star</i>
                     <h2><asp:Label ID="lblCantidadMateriasOfertadas" runat="server" Text=""></asp:Label></h2>
                 </div>
             </div> 
        </div>
        <div class="col s12 m3">
            <div class="card-panel #00897b teal darken-1">
                 <div class="card-content white-text">
                     <asp:Label runat="server" Text="Cantidad Carreras"></asp:Label><i class="material-icons">star</i>
                     <h2><asp:Label ID="lblCantidadCarreras" runat="server" Text=""></asp:Label></h2>
                 </div>
             </div> 
        </div>
        <div class="col s12 m3">
            <div class="card-panel #00897b teal darken-1">
                 <div class="card-content white-text">
                     <asp:Label runat="server" Text="Cantidad Estudiantes"></asp:Label><i class="material-icons">star</i>
                     <h2><asp:Label ID="lblCantidadEst" runat="server" Text=""></asp:Label></h2>
                 </div>
             </div> 
        </div>
        <div class="col s12 m3">
            <div class="card-panel #00897b teal darken-1">
                 <div class="card-content white-text">
                     <asp:Label runat="server" Text="Promedio General"></asp:Label><i class="material-icons">star</i>
                     <h2><asp:Label ID="lblPromedio" runat="server" Text=""></asp:Label></h2>
                 </div>
             </div> 
        </div>
    </div>
</asp:Content>
