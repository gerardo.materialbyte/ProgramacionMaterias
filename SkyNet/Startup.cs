﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SkyNet.Startup))]
namespace SkyNet
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
