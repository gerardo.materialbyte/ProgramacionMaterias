﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SkyNet.Master" AutoEventWireup="true" CodeBehind="RegisterCredential.aspx.cs" Inherits="SkyNet.RegisterCredential" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/md5.js"></script>
    <script>
        function encriptarPassword() {
            var txtP = document.getElementById("ContentPlaceHolder1_txtPassword");
            var pe = calcMD5(txtP.value);
            txtP.value = pe;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col s12 m4 offset-m4">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                        <span>Nombre</span>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="txtApellido" runat="server"></asp:TextBox>
                        <span>Apellido</span>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
                        <span>Nombre de Usuario</span>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <span>Password</span>
                    </div>
                    <div class="row">
                        <asp:Button ID="btnRegistrar" runat="server" Text="Registrar" CssClass="waves-effect btn-large" OnClick="btnRegistrar_Click1"
                            OnClientClick="encriptarPassword()" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
