﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace SkyNet
{
    public partial class Programacion : System.Web.UI.Page
    {
        SkyNetEntity cont = new SkyNetEntity();
        SqlConnection cnx = new SqlConnection();

        public Programacion() {
            cnx.ConnectionString = @"Data Source=DESKTOP-TABFPCA; Initial Catalog=Programacion; Integrated Security=true";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                if (Session["dtProgramacion"] == null)
                {
                    DataTable dtProgramacion = new DataTable();

                    DataColumn c1 = new DataColumn("id_course", Type.GetType("System.String"));
                    DataColumn c2 = new DataColumn("semester", Type.GetType("System.String"));
                    DataColumn c3 = new DataColumn("module", Type.GetType("System.String"));
                    DataColumn c4 = new DataColumn("group_course", Type.GetType("System.String"));
                    DataColumn c5 = new DataColumn("number_enrolled", Type.GetType("System.String"));
                    DataColumn c6 = new DataColumn("classroom", Type.GetType("System.String"));

                    dtProgramacion.Columns.Add(c1);
                    dtProgramacion.Columns.Add(c2);
                    dtProgramacion.Columns.Add(c3);
                    dtProgramacion.Columns.Add(c4);
                    dtProgramacion.Columns.Add(c5);
                    dtProgramacion.Columns.Add(c6);

                    Session["dtProgramacion"] = dtProgramacion;

                }

                if (Request["register"] != null)
                {
                    lblRegistro.Text = Request["register"].ToString();
                    string registro = Request["register"].ToString();
                    cargar(registro);
                    promedio();
                    cursadas();
                    cargarMaterias();
                }
            }
        }

        static int idStudent;
        static int idCareer;

        public void cargar(string registro) {
            var estudiante = from student in cont.students
                             where student.register_number == registro
                             select student;
            idStudent = estudiante.FirstOrDefault().id;
            idCareer = estudiante.FirstOrDefault().career.id;

            lblNombre.Text = estudiante.FirstOrDefault().name;
            lblApellidoPaterno.Text = estudiante.FirstOrDefault().father_lastname;
            lblApellidoMaterno.Text = estudiante.FirstOrDefault().mother_lastname;
            lblFechaNacimiento.Text = estudiante.FirstOrDefault().birthday;
            lblRegistro.Text = estudiante.FirstOrDefault().register_number;
            lblGenero.Text = estudiante.FirstOrDefault().gender;
            lblEmail.Text = estudiante.FirstOrDefault().email;
        }

        public void promedio() {
            var promedioEstudiante = Math.Round((from historyNotes in cont.historyNotes
                                                 where historyNotes.id_student == idStudent
                                                 select historyNotes.note).Average());
            lblPromedio.Text = promedioEstudiante.ToString();
        }

        public void cursadas() {
            var materiasCursadas = (from historyNotes in cont.historyNotes
                                    where historyNotes.id_student == idStudent
                                    select historyNotes).Count();

            lblCursadas.Text = materiasCursadas.ToString();
        }

        public void cargarMaterias() {
            DataTable dt = new DataTable();
            String sql = "SELECT c.name as name " +
                            "FROM careerCourse cc " +
                            "LEFT JOIN(SELECT * FROM historyNotes hn WHERE hn.id_student = " + idStudent + " " +
                            "AND hn.note > 50) hn ON cc.id_course = hn.id_course " +
                            "LEFT JOIN course c ON cc.id_course = c.id " +
                            "WHERE cc.id_career = " + idCareer + " AND hn.note IS NULL";
            SqlDataAdapter adaptador = new SqlDataAdapter(sql, cnx);
            adaptador.Fill(dt);
            ltvMateriasCursadas.DataSource = dt;
            ltvMateriasCursadas.DataBind();
        }

        protected void GVCourses_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void btnCargarMateriasOfertadas_Click(object sender, EventArgs e)
        {
            var materiasOfertadas = from groupsOffered in cont.groupsOffereds
                                    join course in cont.courses on groupsOffered.id_course equals course.id
                                    where groupsOffered.module == txtModule.Text
                                    select new { id_course = course.name, groupsOffered.semester, groupsOffered.module,
                                                    groupsOffered.group_course, groupsOffered.number_enrolled, groupsOffered.classroom};

            GridViewGruposOfertados.DataSource = materiasOfertadas.ToList();
            GridViewGruposOfertados.DataBind();
        }

        protected void GridViewGruposOfertados_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            try
            {
                if (Session["dtProgramacion"] != null)
                {
                    DataTable dtProgramacion = (DataTable)Session["dtProgramacion"];
                    DataRow nuevaFila = dtProgramacion.NewRow();
                    nuevaFila[0] = GridViewGruposOfertados.Rows[e.NewSelectedIndex].Cells[0].Text;
                    nuevaFila[1] = GridViewGruposOfertados.Rows[e.NewSelectedIndex].Cells[1].Text;
                    nuevaFila[2] = GridViewGruposOfertados.Rows[e.NewSelectedIndex].Cells[2].Text;
                    nuevaFila[3] = GridViewGruposOfertados.Rows[e.NewSelectedIndex].Cells[3].Text;
                    nuevaFila[4] = GridViewGruposOfertados.Rows[e.NewSelectedIndex].Cells[4].Text;
                    nuevaFila[5] = GridViewGruposOfertados.Rows[e.NewSelectedIndex].Cells[5].Text;

                    dtProgramacion.Rows.Add(nuevaFila);
                    GridViewProgramacion.DataSource = dtProgramacion;
                    GridViewProgramacion.DataBind();
                    Session["dtProgramacion"] = dtProgramacion;
                }
            }
            catch (Exception)
            {
                throw new Exception("Error");
            }
        }

        protected void GridViewProgramacion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                DataTable dtProgramacion = (DataTable)Session["dtProgramacion"];
                dtProgramacion.Rows.RemoveAt(e.RowIndex);
                GridViewProgramacion.DataSource = dtProgramacion;
                GridViewProgramacion.DataBind();
                Session["dtProgramacion"] = dtProgramacion;
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void btnProgramarSemestre_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["dtProgramacion"] != null)
                {
                    DataTable dtProgramacion = (DataTable)Session["dtProgramacion"];
                    if (dtProgramacion.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtProgramacion.Rows.Count; i++)
                        {
                            student student = encontrar(idStudent);

                            studentCourseRegister courseRegister = new studentCourseRegister();
                            courseRegister.name = dtProgramacion.Rows[i][0].ToString();
                            courseRegister.id_student = student.id;
                            courseRegister.semester = dtProgramacion.Rows[i][1].ToString();
                            courseRegister.module = dtProgramacion.Rows[i][2].ToString();
                            courseRegister.group_course = dtProgramacion.Rows[i][3].ToString();
                            courseRegister.classroom = dtProgramacion.Rows[i][5].ToString();
                            courseRegister.note = 0;
                            courseRegister.state_course = "CONFIRMADA";
                            cont.studentCourseRegisters.Add(courseRegister);
                            cont.SaveChanges();
                        }

                        if (cont.SaveChanges() > 0)
                        {
                            Session.Remove("dtProgramacion");
                            Response.Redirect("Default.aspx");
                        }
                        else
                            throw new Exception("Error al guardar la programacion");
                    }
                    else
                        throw new Exception("No se puede guardar la programacion vacia");
                }
            }
            catch (Exception E)
            {
                throw new Exception(E.Message);
            }
        }

        public student encontrar(int id_student)
        {
            var estudiante = from student in cont.students
                             where student.id == id_student
                             select student;

            return estudiante.FirstOrDefault();
    }
    }
}