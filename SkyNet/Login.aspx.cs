﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SkyNet
{
    public partial class Login : System.Web.UI.Page
    {
        SkyNetEntity cont = new SkyNetEntity();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var datos = cont.sp_loguear(txtUsuario.Text, txtPassword.Text).ToList();
            if (datos.Count > 0)
            {
                Session["credential"] = datos[0].id;
                System.Web.Security.FormsAuthentication.RedirectFromLoginPage(txtUsuario.Text, false);
            }
        }
    }
}