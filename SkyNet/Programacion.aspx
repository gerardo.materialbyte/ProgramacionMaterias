﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SkyNet.Master" AutoEventWireup="true" CodeBehind="Programacion.aspx.cs" Inherits="SkyNet.Programacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            margin-right: 0px;
        }
        .auto-style2 {
            width: 312px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 class="header">Perfil Del Estudiante</h2>
    <div class="row">
          <div class="col s12 m6">
            <div class="card horizontal">
              <div class="card-image">
                <img class="circle" src="http://0.gravatar.com/avatar/cb3ad83a9fe40567b961cdd7fea6f90f?s=320&d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D320&r=G" height="200px" width="200px">
              </div>
              <div class="card-stacked">
                <div class="card-content">
                    <div class="row">
                        <div class="col s12">
                            <b><asp:Label runat="server" Text="NOMBRE:"></asp:Label></b>
                            <asp:Label ID="lblNombre" runat="server" Text=""></asp:Label> <br />
                            <b><asp:Label runat="server" Text="APELLIDO:"></asp:Label> </b>
                            <asp:Label ID="lblApellidoPaterno" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lblApellidoMaterno" runat="server" Text=""></asp:Label> <br />
                            <b><asp:Label runat="server" Text="NUMERO DE REGISTRO:"></asp:Label></b> 
                            <asp:Label ID="lblRegistro" runat="server" Text=""></asp:Label> <br />
                            <b><asp:Label runat="server" Text="FECHA DE NACIMIENTO:"></asp:Label></b>
                            <asp:Label ID="lblFechaNacimiento" runat="server" Text=""></asp:Label> <br />
                            <b><asp:Label runat="server" Text="GENERO:"></asp:Label></b>
                            <asp:Label ID="lblGenero" runat="server" Text=""></asp:Label> <br />
                            <b><asp:Label runat="server" Text="EMAIL:"></asp:Label></b>
                            <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
         <div class="col s12 m3">
             <div class="card-panel #00897b teal darken-1">
                 <div class="card-content white-text">
                     <asp:Label runat="server" Text="Promedio"></asp:Label><i class="material-icons">star</i>
                     <h2><asp:Label ID="lblPromedio" runat="server" Text=""></asp:Label> Pts.</h2>
                 </div>
             </div>             
         </div>
        <div class="col s12 m3">
             <div class="card-panel #00897b teal darken-1">
                 <div class="card-content white-text">
                     <asp:Label runat="server" Text="Materias Cursadas"></asp:Label><i class="material-icons">star</i>
                     <h2><asp:Label ID="lblCursadas" runat="server" Text=""></asp:Label></h2>
                 </div>
             </div>             
         </div>
    </div>

    <div class="row">
        <div class="col s12 m4">
            <div class="card">
                <div class="card-content">
                    <table id="materias">
                        <thead>
                            <tr>
                                <th>Materias No Cursadas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:ListView ID="ltvMateriasCursadas" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%#Eval("name") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col s12 m8">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <div class="col s12 m2">
                             <asp:TextBox ID="txtModule" runat="server" placeholder="Modulo"></asp:TextBox> 
                        </div>
                        <div class="col s12 m5">
                            <asp:Button ID="btnCargarMateriasOfertadas" runat="server" Text="Buscar Materias" 
                                OnClick="btnCargarMateriasOfertadas_Click" CssClass="waves-light btn"/>
                        </div>
                     </div>
                    <div class="row">
                        <asp:GridView ID="GridViewGruposOfertados" runat="server" 
                            AutoGenerateColumns="False" OnSelectedIndexChanging="GridViewGruposOfertados_SelectedIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="id_course" HeaderText="Materia" />
                                <asp:BoundField DataField="semester" HeaderText="Semestre" />
                                <asp:BoundField DataField="module" HeaderText="Modulo" />
                                <asp:BoundField DataField="group_course" HeaderText="Grupo" />
                                <asp:BoundField DataField="number_enrolled" HeaderText="Inscritos" />
                                <asp:BoundField DataField="classroom" HeaderText="Aula" />
                                <asp:CommandField ButtonType="Button" HeaderText="Programar" SelectText="Agregar" 
                                    ShowSelectButton="True" ControlStyle-CssClass="waves-effect btn" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <asp:GridView ID="GridViewProgramacion" runat="server" 
                        AutoGenerateColumns="False" OnRowDeleting="GridViewProgramacion_RowDeleting">
                        <Columns>
                            <asp:BoundField DataField="id_course" HeaderText="Materia" />
                            <asp:BoundField DataField="semester" HeaderText="Semestre" />
                                <asp:BoundField DataField="module" HeaderText="Modulo" />
                                <asp:BoundField DataField="group_course" HeaderText="Grupo" />
                                <asp:BoundField DataField="number_enrolled" HeaderText="Inscritos" />
                                <asp:BoundField DataField="classroom" HeaderText="Aula" />
                                <asp:CommandField ButtonType="Button" DeleteText="Borrar" 
                                    ShowDeleteButton="True" ControlStyle-CssClass="waves-effect btn" />
                        </Columns>
                    </asp:GridView>
                    <asp:Button ID="btnProgramarSemestre" runat="server" Text="Programar Semestre" CssClass="waves-effect btn" OnClick="btnProgramarSemestre_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
